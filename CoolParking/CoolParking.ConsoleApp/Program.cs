﻿using CoolParking.ConsoleApp.Menu;
using System;

namespace CoolParking.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            StartMenu menu = new StartMenu();
            menu.MainMenu();
        }
    }
}
