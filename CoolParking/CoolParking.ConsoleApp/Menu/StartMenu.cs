﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System;


namespace CoolParking.ConsoleApp.Menu
{
    class StartMenu
    {
        private readonly ParkingMenu parkingMenu;
        private readonly SettingMenu settingMenu;
        private readonly ShowMenu showMenu;

        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly IParkingService _parkingService;
        public StartMenu()
        {
            _withdrawTimer = new TimerService(Settings.intervalForPay * 1000);
            _logTimer = new TimerService(Settings.intervalWriteIntoLog * 1000);
            _parkingService = new ParkingService(_withdrawTimer, _logTimer, new LogService("Transactions.log"));
            
            showMenu = new ShowMenu();
            parkingMenu = new ParkingMenu(_parkingService);
            settingMenu = new SettingMenu();
        }
        public void MainMenu()
        {
            showMenu.DisplayMainMenu();
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.D1)
                {
                    showMenu.DisplayMainMenu();
                    ParkingMenu();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D2)
                {
                    showMenu.DisplaySettingsMenu();
                    SettingsMenu();
                    break;
                }
            }
        }
        private void ParkingMenu()
        {
            showMenu.DisplayParkingMenu();
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.D1)
                {
                    parkingMenu.ParkingBalance();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D2)
                {
                    parkingMenu.SumLastParkingTransactions();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D3)
                {
                    parkingMenu.InfoParkingSpaces();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D4)
                {
                    parkingMenu.LastParkingTransactions();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D5)
                {
                    parkingMenu.ReadTransactionsLog();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D6)
                {
                    parkingMenu.AllVehicles();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D7)
                {
                    parkingMenu.AddVehicle();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D8)
                {
                    parkingMenu.RemoveVehicle();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D9)
                {
                    parkingMenu.TopUpBalance();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.Escape)
                {
                    MainMenu();
                    break;
                }
            }
            ParkingMenu();
        }
        private void SettingsMenu()
        {
            showMenu.DisplaySettingsMenu();
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.D1)
                {
                    settingMenu.ChengeBalance();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D2)
                {
                    settingMenu.ChengeMaxSize();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D3)
                {
                    settingMenu.ChengePaymentInterval();
                    _withdrawTimer.Interval = Settings.intervalForPay * 1000;
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D4)
                {
                    settingMenu.ChengeWriteToLogInterval();
                    _logTimer.Interval = Settings.intervalWriteIntoLog * 1000;
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D5)
                {
                    settingMenu.ChengeFine();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.D6)
                {
                    settingMenu.ChengeCostVehicle();
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.Escape)
                {
                    MainMenu();
                    break;
                }
            }
            SettingsMenu();
        }
    }
}
