﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Parking 
    {
        private static readonly Lazy<Parking> lazy
            = new Lazy<Parking>(() => new Parking());

        public static Parking Instance
            => lazy.Value;
        
        private Parking() 
        {
            Balance = 0;
            ListVehicles = new List<Vehicle>();
        }

        public decimal Balance { get; set; }
        public List<Vehicle> ListVehicles { get; set; }
        public void Clear()
        {
            ListVehicles.Clear();
            Balance = 0;
        }
    }
}