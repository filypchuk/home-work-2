﻿using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public DateTime Time { get; private set; }
        public string VehicleId { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Sum { get; private set; }
        public TransactionInfo(DateTime time, string vehicleId, VehicleType vehicleType, decimal cost)
        {
            Time = time;
            VehicleId = vehicleId;
            VehicleType = vehicleType;
            Sum = cost;
        }

    }
}