﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService: ILogService
    {
        public string LogPath { get; }
        public LogService(string logPath)
        {
            LogPath = logPath;
        }
        public void Write(string logInfo)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(LogPath, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(logInfo);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public string Read()
        {
            string transactionsLog = "";
            try
            {
                using (StreamReader str_reader = new StreamReader(LogPath))
                {
                    string line;
                    while ((line = str_reader.ReadLine()) != null)
                    {
                        transactionsLog += $"{line}\r\n";
                    }
                }
            }
            catch (FileNotFoundException)
            {
                throw new InvalidOperationException("File not found");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return transactionsLog;
        }
    }
}